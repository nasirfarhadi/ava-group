import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';


import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { TraidingData } from './view/account/common/data/traiding.data';
import { AuthGuard } from './core/user/auth.guard';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path:'account',
    canActivate: [AuthGuard],
    loadChildren: () =>
    import('./view/account/account.module').then(m => m.AccountModule)
  }, 
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    InMemoryWebApiModule.forRoot(TraidingData, { delay: 1000 }),
  ],
  declarations:[],
  exports: [RouterModule]
})
export class AppRoutingModule { }
