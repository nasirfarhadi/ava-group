import { Component, Renderer2, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, RoutesRecognized } from '@angular/router';
import { mergeMap, filter, map } from 'rxjs/operators';
import { CommonService } from './common/service/common.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit{
  title = 'avaGroup';
  constructor(private renderer: Renderer2, 
    private router: Router, 
    private activatedRoute: ActivatedRoute,
    private common: CommonService,
    private translateService: TranslateService) {
      // Set fallback language
    translateService.setDefaultLang('en');
    // Supported languages
    translateService.addLangs(['en', 'de']); 
  }
  
  ngOnInit(){
    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .pipe(map(() => this.activatedRoute))
      .pipe(map((route) => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }))
      .pipe(filter((route) => route.outlet === 'primary'))
      .pipe(mergeMap((route) => route.data))
      .subscribe((data) => this.updateBodyClass(data.bodyClass));
  }

  private updateBodyClass(customBodyClass?: string) {
    this.renderer.setAttribute(document.body, 'class', '');
    if (customBodyClass) {
      this.renderer.addClass(document.body, customBodyClass);
    }
  }

}
