import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent} from './app.component';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { AccountModule } from './view/account/account.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { TraidingData } from './view/account/common/data/traiding.data';
import { HomeModule } from './view/home/home.module';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AuthGuard } from './core/user/auth.guard';
import { AuthService } from './core/user/common/service/auth.service';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { WebpackTranslateLoader } from './common/translate';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    AppRoutingModule,
    SharedModule,
    CoreModule,
    AccountModule,
    HomeModule,
    HttpClientModule,
    InMemoryWebApiModule.forRoot(TraidingData, { delay: 1000 }),  
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        // useFactory: createTranslateLoader,
        // deps: [HttpClient]
        useClass: WebpackTranslateLoader
      }
  })
  ],
  declarations: [
    AppComponent,
  ],
  providers: [
     AuthGuard,
     AuthService
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
