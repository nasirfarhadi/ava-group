import {Directive, HostBinding, HostListener} from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {
  @HostBinding('class.show') isShow = false;

  constructor() {
  }

  @HostListener('click', ['$event']) toggleOpen(e: any) {
    e.preventDefault();
    this.isShow = !this.isShow;
    this.isShow ? e.target.nextElementSibling.setAttribute('class', 'show dropdown-menu')
      : e.target.nextElementSibling.setAttribute('class','dropdown-menu');
  }

}