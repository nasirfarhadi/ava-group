import { Directive, Renderer2, ElementRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Directive({
  selector: '[appOrangeColor]'
})
export class OrangeColorDirective implements OnInit {

  constructor(private renderer: Renderer2, 
        private elmRef: ElementRef,
        private router: Router) { }

  ngOnInit(){
    let currentUrl = this.router.url;
    console.log(currentUrl)
    this.renderer.addClass(this.elmRef.nativeElement, 'orange-color')
  }
}
