import {NgModule} from "@angular/core";
import {DropdownDirective} from "./dropdown.directive";
import { OrangeColorDirective } from './orange-color.directive';

@NgModule({
  imports: [],
  exports: [
    DropdownDirective,
    OrangeColorDirective
  ],
  declarations: [
    DropdownDirective,
    OrangeColorDirective,

  ],
})

export class UtilsModule {
};
