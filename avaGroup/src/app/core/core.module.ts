import {NgModule} from "@angular/core";
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NavbarComponent} from './navbar/navbar.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {AccountMenuComponent} from './account-menu/account-menu.component';
import {LanguageComponent} from './language/language.component';
import { MessageModule } from './message/message.module';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { UserModule } from './user/user.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UtilsModule } from '../common/directives/utils-module';
import { TranslateModule } from '@ngx-translate/core';
import { WebpackTranslateLoader } from '../common/translate';

@NgModule({
  imports: [
    CommonModule, 
    RouterModule,
     UtilsModule,
     MessageModule,
     UserModule,
     FormsModule,
     ReactiveFormsModule,
     TranslateModule.forChild({
        loader: {
          provide: TranslateModule,
          useClass: WebpackTranslateLoader
        }
      })
    ],
  exports: [
    NavbarComponent,
    SidebarComponent,
    AccountMenuComponent,
    BreadcrumbComponent,
    TranslateModule
  ],
  declarations: [
    NavbarComponent,
    SidebarComponent,
    AccountMenuComponent,
    LanguageComponent,
    BreadcrumbComponent,
  ],
  providers: []
})

export class CoreModule {
}
