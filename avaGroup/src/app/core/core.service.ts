import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CoreService {
  public navbarOpen: boolean = false;
  constructor() { }
  sidebarShow(): void{
    this.navbarOpen = true
  }

  sidebarHide(): void{
    this.navbarOpen = false
  }
}
