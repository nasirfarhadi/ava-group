import { Component, OnInit, ElementRef, ViewChild, ViewChildren } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DropdownDirective } from 'src/app/common/directives/dropdown.directive';

@Component({
  selector: 'app-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.css']
})
export class LanguageComponent implements OnInit {
  imgPath: string = './assets/img/us-flag.svg';
  @ViewChildren(DropdownDirective) dirs;
  constructor(private translateService: TranslateService,
              private elm: ElementRef) {
   }

  ngOnInit() {
  }

  switchLanguage(language: string) {
    this.translateService.use(language);
    if(language === 'en') {
       this.imgPath = "./assets/img/us-flag.svg"
      } else if(language === 'de') {
        this.imgPath = "./assets/img/de-flag.svg"        
    }
  }

}
