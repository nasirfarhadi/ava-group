import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MessageComponent } from './message.component';



@NgModule({
  declarations: [
    MessageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path:'message',
        component: MessageComponent,
        outlet: 'popup'
      }
    ])
  ]
})
export class MessageModule { }
