import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  private message:string[] = [];
  isDisplayed = false;

  constructor() { }

  get messages(): string[] {
    return this.message;
  }

  addMessage(message: string): void {
    const currentDate = new Date();
    this.message.unshift(message + 'at' + currentDate.toLocaleString());
  }

}
