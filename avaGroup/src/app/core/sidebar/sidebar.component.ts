import { Component, OnInit, ElementRef } from '@angular/core';
import { CoreService } from '../core.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
})
export class SidebarComponent implements OnInit {
  
  constructor(public coreService:CoreService,
              private el: ElementRef) { }

  ngOnInit() {
  }

}
