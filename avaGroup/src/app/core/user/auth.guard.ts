import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, UrlSegment, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './common/service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService,
              private router: Router){}

  canActivate(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean{
    return this.authService.isAuthenticated("userName", 'password').then(
        (authentication: boolean)=>{
            if(authentication){
                return true;
            }else{
                this.router.navigate(['/']);
            }
        }
    );
}
  
}
