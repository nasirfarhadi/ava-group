import { Injectable } from '@angular/core';
import { User } from '../model/user.model';
import { MessageService } from 'src/app/core/message/message.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  currentUser: User;
  redirectUrl: string;
  loggedIn = false;

  get isLoggedIn(): boolean {
    return !!this.currentUser;
  }

  constructor(private messageService: MessageService,
              private router: Router) { }


  isAuthenticated(userName: string, password: string){
      const promise = new Promise(
          (resolve, reject) =>{
              setTimeout(()=>{
                  resolve(this.loggedIn)
                  if (!userName || !password) {
                        this.messageService.addMessage('Please enter your userName and password');
                        return;
                      }
                      if (userName === 'admin') {
                        this.currentUser = {
                          id: 1,
                          userName,
                          isAdmin: true
                        };
                        this.messageService.addMessage('Admin login');
                        return;
                     }
                      this.currentUser = {
                        id: 2,
                        userName,
                        isAdmin: false
                      };
                      this.messageService.addMessage(`User: ${this.currentUser.userName} logged in`);
              },400)
          }
      )
      return promise
  }

  logIn(){
      this.loggedIn = true;
  }

}
