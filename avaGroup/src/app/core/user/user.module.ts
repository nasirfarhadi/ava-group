import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from 'src/app/view/home/home.component';
import { TranslateModule } from '@ngx-translate/core';
import { WebpackTranslateLoader } from 'src/app/common/translate';



@NgModule({
  declarations: [
    LoginComponent
  ],
  exports: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {path:'home',component: HomeComponent}
    ]),
    TranslateModule.forChild({
      loader: {
        provide: TranslateModule,
        useClass: WebpackTranslateLoader
      }
    })
  ]
})
export class UserModule { }
