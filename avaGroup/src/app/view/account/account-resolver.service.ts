import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable,of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { TraidingService } from './common/services/traiding.service';
import { TraidingResolved } from './common/model/traiding.model';
@Injectable({
  providedIn: 'root'
})
export class AccountResolverService implements Resolve<TraidingResolved> {

  constructor(private traidingService: TraidingService) { }
  
  resolve(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<TraidingResolved> {
      const id = route.paramMap.get('id');
      console.log(id)
      if (isNaN(+id)) {
      const message = `Traiding id was not a number: ${id}`;
      console.error(message);
      return of({ Traiding: null, error: message });
      }

      return this.traidingService.getItemTriding(+id)
      .pipe(
        map(Traiding => ({ Traiding })),
        catchError(error => {
          const message = `Retrieval error: ${error}`;
          console.error(message);
          return of({ Traiding: null, error: message });
        })
      );
      }
}
