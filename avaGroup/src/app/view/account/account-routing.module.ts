import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './component/profile/profile.component';
import { SidebarAccountComponent } from './component/sidebar-account/sidebar-account.component';
import { TraidingComponent } from './component/traiding/traiding.component';
import { AccountComponent } from './account.component';
import { Routes,RouterModule } from '@angular/router';
import { TraidingItemComponent } from './component/traiding/traiding-item/traiding-item.component';
import { AccountResolverService } from './account-resolver.service';
import { AccountProfileComponent } from './component/profile/account-profile/account-profile.component';
import { TraidingCrudComponent } from './component/traiding/traiding-crud/traiding-crud.component';
import { TraidingEditComponent } from './component/traiding/traiding-edit/traiding-edit.component';
import { CoreModule } from 'src/app/core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TraidingData } from './common/data/traiding.data';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { AuthGuard } from 'src/app/core/user/auth.guard';


const routes: Routes = [
  {
    path:'account/my-account',
    data: { bodyClass: 'orange-color' },
    component:AccountComponent
  },
  {
    path:'account/new',
    data: { bodyClass: 'orange-color' },
    component:TraidingEditComponent,
  },
  {
    path:'account/:id/edit',
    data: { bodyClass: 'orange-color' },
    component:TraidingEditComponent,
    resolve: {resolvedData: AccountResolverService} 
  }

];

@NgModule({
  declarations: [
    ProfileComponent,
    SidebarAccountComponent,
    TraidingComponent,
    AccountComponent,
    TraidingItemComponent,
    AccountProfileComponent,
    TraidingCrudComponent,
    TraidingEditComponent
  ],
  exports:[
    TraidingComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CoreModule,
    FormsModule,
    ReactiveFormsModule,
    InMemoryWebApiModule.forRoot(TraidingData, { delay: 1000 })
  ]
})
export class AccountRoutingModule { }