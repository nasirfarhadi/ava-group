import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountRoutingModule } from './account-routing.module';
import { UtilsModule } from 'src/app/common/directives/utils-module';
import { TranslateModule } from '@ngx-translate/core';
import { WebpackTranslateLoader } from 'src/app/common/translate';


@NgModule({
  declarations: [    
  ],
  imports: [
    CommonModule,
    AccountRoutingModule,
    UtilsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateModule,
        useClass: WebpackTranslateLoader
      }
    })
  ]
})
export class AccountModule { }