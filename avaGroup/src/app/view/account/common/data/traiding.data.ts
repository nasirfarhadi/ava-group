import { InMemoryDbService } from 'angular-in-memory-web-api';
import { TraidingModel } from '../model/traiding.model';

export class TraidingData implements InMemoryDbService{
createDb(): {traidings: TraidingModel[]} {
    const traidings: TraidingModel[] = [
        {
            id: 1,
            cabinNumber: 212333565,
            traidingAccount: "410018138953253",
            Published: true,
            status: true
        },{
            id: 2,
            cabinNumber: 212333561,
            traidingAccount: "410018138953251",
            Published: true,
            status: true
        },{
            id: 3,
            cabinNumber: 212333565,
            traidingAccount: "410018138955253",
            Published: true,
            status: true
        },{
            id: 4,
            cabinNumber: 212333565,
            traidingAccount: "410018135953253",
            Published: true,
            status: true
        },{
            id: 5,
            cabinNumber: 212333565,
            traidingAccount: "410018136953253",
            Published: true,
            status: true
        },{
            id: 6,
            cabinNumber: 212333565,
            traidingAccount: "410018338953253",
            Published: true,
            status: true
        },{
            id: 7,
            cabinNumber: 212333565,
            traidingAccount: "410016138953253",
            Published: true,
            status: false
        },{
            id: 8,
            cabinNumber: 212333565,
            traidingAccount: "410018688953253",
            Published: true,
            status: false
        }
    
    ];
    return {traidings}
}
} 
