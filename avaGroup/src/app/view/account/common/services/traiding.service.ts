import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

import { TraidingModel } from '../model/traiding.model';

@Injectable({
  providedIn: 'root'
})
export class TraidingService {
  
  private baseUrl = 'api/traidings';

  constructor(private http: HttpClient) { }

  getTraiding() : Observable<TraidingModel[]> {
    return this.http.get<TraidingModel[]>(this.baseUrl)
    .pipe(
      tap(data => console.log(JSON.stringify(data))),
      catchError(this.handleError)
    )
  }

  getItemTriding(id: number): Observable<TraidingModel> {
    if (id === 0) {
      return of(this.initializeProduct());
    }
    const url = `${this.baseUrl}/${id}`;
    return this.http.get<TraidingModel>(url)
      .pipe(
        tap(data => console.log('getTraiding: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  creatTraiding(traiding:TraidingModel): Observable<TraidingModel>{
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    traiding.id =null;
    return this.http.post<TraidingModel>(this.baseUrl,traiding,{headers})
    .pipe(
      tap(data => console.log('createTraiding:' + JSON.stringify(data))),
      catchError(this.handleError)
    )
  }

  updateTraiding(traiding: TraidingModel): Observable<TraidingModel> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.baseUrl}/${traiding.id}`;
    return this.http.put<TraidingModel>(url, traiding, { headers })
      .pipe(
        tap(() => console.log('updateTraiding: ' + traiding.id)),
        // Return the product on an update
        map(() => traiding),
        catchError(this.handleError)
      );
  }

  private handleError(err: any): Observable<never> {
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Backend returned code ${err.status}: ${err.body.error}`;
    }
    console.error(err);
    return throwError(errorMessage);
  }
  
  private initializeProduct(): TraidingModel {
    // Return an initialized object
    return {
      id: 0,
      cabinNumber: null,
      traidingAccount: null,
      Published: true,
      status: true,
    };
  }


}
