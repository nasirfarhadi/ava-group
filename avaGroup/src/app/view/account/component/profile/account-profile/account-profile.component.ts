import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/user/common/service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account-profile',
  templateUrl: './account-profile.component.html',
  styleUrls: ['./account-profile.component.css']
})
export class AccountProfileComponent implements OnInit {

  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    setTimeout(()=>{
      if(this.userName === ''){      
        this.router.navigate(['/']);
      }
    },500)
    
  }

  get isLoggedIn(): boolean {
    return this.authService.isLoggedIn;
  }

  get userName(): string {
    if (this.authService.currentUser) {
      return this.authService.currentUser.userName;
    }
    return '';
  }

}
