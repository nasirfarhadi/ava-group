import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraidingCrudComponent } from './traiding-crud.component';

describe('TraidingCrudComponent', () => {
  let component: TraidingCrudComponent;
  let fixture: ComponentFixture<TraidingCrudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraidingCrudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraidingCrudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
