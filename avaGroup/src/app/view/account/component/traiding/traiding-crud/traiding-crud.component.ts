import { Component, OnInit } from '@angular/core';
import { TraidingService } from '../../../common/services/traiding.service';
import { MessageService } from 'src/app/core/message/message.service';
import { ActivatedRoute, Router, RoutesRecognized } from '@angular/router';
import { TraidingModel, TraidingResolved } from '../../../common/model/traiding.model';
import { FormGroup, FormControl, Validators, FormBuilder  } from '@angular/forms';
@Component({
  selector: 'app-traiding-crud',
  templateUrl: './traiding-crud.component.html',
  styleUrls: ['./traiding-crud.component.css']
})
export class TraidingCrudComponent implements OnInit {
  pageTitle: string;
  errorMessage: string;

  public showDialog =false;

  status: boolean;
  published = [false, true];
  traidingForm: FormGroup;
  ITraiding: TraidingModel;
  statusTraiding:boolean = true;
  
  private currentTraiding: TraidingModel;
  private originalTraiding: TraidingModel;

  get traiding(): TraidingModel{
    return this.currentTraiding;
  }

  set traiding(value: TraidingModel){
    this.currentTraiding = value;
    this.originalTraiding = value ? {...value} : null;
  }

  constructor(private fb: FormBuilder,
              private traidingService: TraidingService,
              private messageService: MessageService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.traidingForm = this.fb.group({
      cabinNumber: ['', Validators.required],
      traidingAccount: ['', Validators.required],
      published:[false]
    })

    this.route.data.subscribe(data => {
      const resolvedData: TraidingResolved = data['resolvedData'];
      this.errorMessage = resolvedData.error;
      this.onProductRetrieved(resolvedData.Traiding);
    });    
    this.route.paramMap.subscribe(params => {
      const trdID =  +params.get('id');
      if(trdID) {
        this.pageTitle = 'Edit Account';
        this.getTraiding(trdID);
        this.reset();
      } else {
        this.pageTitle = 'New Account';
        this.ITraiding = {
          id: null,
          cabinNumber: null,
          traidingAccount:'',
          Published: true,
          status: true
        }
      }
    })
  }

   getTraiding(id: number){
     this.traidingService.getItemTriding(id).subscribe(
      (traiding: TraidingModel) => {
        this.editTraiding(traiding),
        this.ITraiding = traiding ,
        this.statusTraiding = this.ITraiding.status;
        console.log('statusTraiding:' + this.statusTraiding);
        console.log('ITraiding.status:' + this.ITraiding.status);
      },
      (err: any) => console.log(err)
     );
   }
   editTraiding(traiding: TraidingModel){
    this.traidingForm.patchValue({
      cabinNumber: traiding.cabinNumber,
      traidingAccount: traiding.traidingAccount,
      published: traiding.Published
    });
   }

  onProductRetrieved(traiding: TraidingModel): void {
    this.traiding = traiding;

    if (!this.traiding) {
      this.pageTitle = 'No product found';
    } else {
      if (this.traiding.id === 0) {
        this.pageTitle = 'Add traiding';
      } else {
        this.pageTitle = `Edit traiding: ${this.traiding.cabinNumber}`;
      }
    }
  }

  reset(): void {
    this.currentTraiding = null;
    this.originalTraiding = null;
  }
 
  saveTraiding(): void {
    this.mapFormValuesToTraidingModel();
    if(this.ITraiding.id){
      this.traidingService.updateTraiding(this.ITraiding).subscribe(
        () => this.onSaveComplete(`The new ${this.traidingForm.value.cabinNumber} was saved`),
        (err: any) => this.errorMessage = err
      );
    } else {
      this.traidingService.creatTraiding(this.ITraiding).subscribe(
        () => this.onSaveComplete(`The new ${this.traidingForm.value.cabinNumber} was saved`),
        (err: any) => this.errorMessage = err
      );
    }
  }

  mapFormValuesToTraidingModel(){
    this.ITraiding.cabinNumber = this.traidingForm.value.cabinNumber;
    this.ITraiding.traidingAccount = this.traidingForm.value.traidingAccount;
    this.ITraiding.Published = this.traidingForm.value.Published;
  }

  onSaveComplete(message?: string): void {
    if (message) {
      this.messageService.addMessage(message);
    }
    this.reset();
    // Navigate back to the product list
    this.router.navigate(['/account/my-account']);
  }


  openDialog(){
    this.showDialog = !this.showDialog;
  } 

  

}
