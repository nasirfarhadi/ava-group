import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TraidingEditComponent } from './traiding-edit.component';

describe('TraidingEditComponent', () => {
  let component: TraidingEditComponent;
  let fixture: ComponentFixture<TraidingEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TraidingEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TraidingEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
