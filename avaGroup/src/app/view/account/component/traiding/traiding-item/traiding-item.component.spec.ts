import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraidingItemComponent } from './traiding-item.component';

describe('TraidingItemComponent', () => {
  let component: TraidingItemComponent;
  let fixture: ComponentFixture<TraidingItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraidingItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraidingItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
