import { Component, OnInit } from '@angular/core';
import { TraidingModel } from '../../../common/model/traiding.model';
import { TraidingService } from '../../../common/services/traiding.service';

@Component({
  selector: 'app-traiding-item',
  templateUrl: './traiding-item.component.html',
  styleUrls: ['./traiding-item.component.css'],
  providers: [TraidingService]
})
export class TraidingItemComponent implements OnInit {
  ITraiding: TraidingModel[] = [];
  errorMessage = '';
  isShow= false;

  constructor(private traidingService : TraidingService) {
   }

  ngOnInit() {
    this.traidingService.getTraiding().subscribe({
      next: traiding => {
        this.ITraiding = traiding;
      },
      error: err => this.errorMessage = err
    })
  }

  toggleDisplay() {
    this.isShow = !this.isShow;
  }

}
