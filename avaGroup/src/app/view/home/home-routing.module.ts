import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from 'src/app/core/core.module';
import { HomeComponent } from './home.component';
import { RouterModule, Routes } from '@angular/router';
import { UserModule } from 'src/app/core/user/user.module';
import { UtilsModule } from 'src/app/common/directives/utils-module';

const routes: Routes = [
  {
    path:'home',
    component:HomeComponent 
  }
];

@NgModule({
  declarations: [
    HomeComponent
  ],
  exports:[
    HomeComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    CoreModule,
    UtilsModule,
    UserModule
  ]
})
export class HomeRoutingModule { }
